import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NeweditorComponent } from './neweditor/neweditor.component';

const routes: Routes = [
  {path:'newedit',component:NeweditorComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NeweditorRoutingModule { }

import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {
@Input() order:any;
  constructor() { }

  ngOnInit(): void {
    console.log(this.order)
  }

}

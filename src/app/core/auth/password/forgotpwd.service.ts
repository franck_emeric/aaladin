import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PasswordforData} from '../model';
import { environment } from 'src/environments/environment.prod';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ForgotpwdService {

  constructor(private http:HttpClient) { }

  passwordforgot(data:PasswordforData,user:number):Observable<any>{
  
    return this.http.post(environment.apiBaseUrl+user,data);

  }

veryfyemail(email:string){
let user:any
let error:any
 this.http.post(environment.apiBaseUrl,{email:email}).subscribe(res=>{
    user=res;
  },
  err=>{
  error=err
  });
  if(error!=null){
   return error;
  }
  return user; 
}
}

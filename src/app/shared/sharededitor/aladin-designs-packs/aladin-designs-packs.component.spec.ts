import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinDesignsPacksComponent } from './aladin-designs-packs.component';

describe('AladinDesignsPacksComponent', () => {
  let component: AladinDesignsPacksComponent;
  let fixture: ComponentFixture<AladinDesignsPacksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinDesignsPacksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinDesignsPacksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

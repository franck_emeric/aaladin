  import { Component, OnInit,EventEmitter,Output, Input,OnDestroy,OnChanges } from '@angular/core';
  import { charAtIndex } from 'pdf-lib';
  import { LocalService ,AladinService, CartService} from 'src/app/core';
  import { ObservableserviceService } from 'src/app/core/rxjs/observableservice.service';
  declare  var require:any;
  var myalert=require('sweetalert2');
  var $ = require("jquery");
  import { Subscription } from 'rxjs';

  @Component({
    selector: 'app-aladin-designs-clothes',
    templateUrl: './aladin-designs-clothes.component.html',
    styleUrls: ['./aladin-designs-clothes.component.scss']
  })
  export class AladinDesignsClothesComponent implements OnInit,OnDestroy,OnChanges {
    price:any=0;
    unit_price=0;
    @Input() face1:any;
    @Input() face2:any;
    data:any[]=[];
    mydesigns:any=[];
    inpnmb =1;
    prod:any=[];
    active_prod:any;
    hide_calco=true;
    show_modal=true;
    active=false;
    size={
      m:false,
      l:false,
      xl:false,
      xxl:false
    }
    type:any;
    subscription:any=Subscription;
    shape:any;
    cart:any;
    activModal:any;
    @Output() newItemEvent =new EventEmitter<string>();
    @Output() deleteEvent = new EventEmitter<any>();

    constructor(private localservice:LocalService,private service:CartService,private aladin:AladinService) { }

    ngOnInit(): void {
    
        this.Updatedata()
        this.Initdata()

    }

    Updatedata=()=>{
      this.service.dignUpdated.subscribe(message=>{
        if(message){
          this.mydesigns=this.localservice.items;
          if(this.mydesigns.length>0){
            if(this.data.length>0){
              this.data=[]
            }  
            for(let item of this.mydesigns){
            if(item.data.category=="clothes"){
              this.data.push(item);
      
            }
          
            }
          }
        }

      })
    
    }


    Initdata=()=>{
      this.mydesigns=this.localservice.items;
      console.log(this.mydesigns[0].data.category)
      if(this.mydesigns.length>0){
        if(this.data.length>0){
          this.data=[]
        }  
        for(let item of this.mydesigns){
        if(item.data.category=="clothes"){
          this.data.push(item);

        }
      
        }
      }
      console.log(this.data.length);
    }


    ngOnChanges(){
    

    }

    ngOnDestroy(){
    // this.subscription.unsubscribe()
    }

    getPrice(){
      let price= 5100// this.unit_price 
      this.inpnmb =+this.inpnmb+1;
        if(this.inpnmb>0&&price!=null){
          this.price= this.inpnmb*(+price);
        }
      }

    getPricem(){
      let price= 5100//this.unit_price 
      this.inpnmb=this.inpnmb-1;
    if(this.inpnmb>0 && price!=null){
      if(((+this.inpnmb)*(+this.price))>0){
        this.price= ((+this.inpnmb)*(+price));
      }
    }else{
      this.inpnmb=this.inpnmb+1;

    }
      }

      decremet_by(){
        let price= 5100// this.unit_price 
        this.inpnmb =(+this.inpnmb)*5;
        if(this.inpnmb>0&&price!=null){
          this.price= this.inpnmb*(+price);
        }

      }

      divide(){
        let price= 5100//this.unit_price 
        if(this.inpnmb >5){
          this.inpnmb =~~((+this.inpnmb)/5);
          if(this.inpnmb> 0 && price!=null){
            this.price= this.inpnmb*(+price);
          }
        }
      }

      removeOneItem(event:any){
        let id=event.target.id;
        let cart = charAtIndex(id,2);
        let prod=charAtIndex(id,0);
        if(+cart[0]!=-1){
          this.data.splice(+prod[0],1);
          this.localservice.removeOne(cart[0]);
          console.log(this.data,cart[0])
        }
        
    }



    OnsizeChange(event:any){
      if(event.target.value!="value"){
        if(event.target.value=="L"){
          this.size.l=true;

        }
        if(event.target.value=="M"){
          this.size.m=true
        }
        if(event.target.value=="XL"){
          this.size.xl=true
        }

        if(event.target.value=="XXL"){
          this.size.xxl=true
        }

      }

    }
    
    removeItem(id:any){
      if(id!=-1){
        this.localservice.removeOne(id);
      }
      
    }

    addtocart(event:any){
      $("body").children().first().before($(".modal"));
      console.log(this.active_prod)
      let data = this.getId(+this.active_prod);
      if(data!=-1){
        let cart_data={
          qty:this.inpnmb,
          price: 5100 ,//this.unit_price,
          t: this.price ,
          id:data.id,
          face1:data.face1,
          face2:data.face2,
         
          shape:this.shape,
          type:this.type,
          size:this.size,
          //name:data.name,
          //production_f1:data.production_f1,
          //production_f2:data.production_f2,
          type_product:"editor",
          category:"clothes"

        };

    try{
      this.localservice.adtocart(cart_data);
      this.data.splice(+this.active_prod,1);
      this.removeItem(this.cart[0]);

      if(this.data.length==1){
        setTimeout(()=>{
          location.reload()

        },1000)
      let el:any=document.getElementById(this.activModal)
      this.aladin.triggerMouse(el);
       myalert.fire({
        title:'<strong>produit ajouté</strong>',
        icon:'success',
        html:
          '<h6 style="color:blue">Felicitation</h6> ' +
          '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
          '<a href="/cart">Je consulte mon panier</a>' 
          ,
        showCloseButton: true,
        focusConfirm: false,
      
      })

      }else{
        let el:any=document.getElementById(this.activModal)
        this.aladin.triggerMouse(el);
         myalert.fire({
          title:'<strong>produit ajouté</strong>',
          icon:'success',
          html:
            '<h6 style="color:blue">Felicitation</h6> ' +
            '<p style="color:green">Votre design a été ajouté dans le panier</p> ' +
            '<a href="/cart">Je consulte mon panier</a>' 
            ,
          showCloseButton: true,
          focusConfirm: false,
        
        })
      }
    
    
      

    }catch(e:any){
        console.log(e)
    }


      }else{
        myalert.fire({
          title: "Erreur",
          text: "Une erreur s'est produite",
          icon: "error",
          button: "Ok"
          })
      }
    }

  toggle(){
  this.active=!this.active
  }

  hidemodal(){
    this.hide_calco=!this.hide_calco;

  }

  OnTypeChange(event:any){
    console.log(event.target.value);
    this.shape=event.target.value;
  }

  OnShapeChange(event:any){
    console.log(event.target.value);
    this.type=event.target.value;
  }

  getProdprice=(event:any)=>{
    let id=event.target.id
    this.activModal=id;
    this.cart = charAtIndex(id,0);
    let product= charAtIndex(id,2)
    this.unit_price=parseInt(event.target.name)
    this.price=this.unit_price
    this.inpnmb=1;
    this.active_prod= product[0];
    console.log(event.target.name);
    this.getId(this.active_prod);

  }

  getId=(id:any)=>{
    if(+id!=-1){
      let data=this.data[+id];
      return data;
    }else{
      return -1;
    }

  }

  cartlength(value:any){
    this.newItemEvent.emit(value);

  }


  }

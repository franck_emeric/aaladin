import { Component, OnInit,Input } from '@angular/core';
import { HttpService, ListService,LocalService } from 'src/app/core';
import { NeweditorService } from 'src/app/core';
import { fabric } from 'fabric';
import { ColorEvent } from 'ngx-color';
import { DomSanitizer } from '@angular/platform-browser';
declare var require :any
var $ = require("jquery")
@Component({
  selector: 'app-aladin',
  templateUrl: './aladin.component.html',
  styleUrls: ['./aladin.component.scss']
})
export class AladinComponent implements OnInit {

  constructor(private Editor:NeweditorService,private http:HttpService, private sanitizer:DomSanitizer,private forms:ListService, private local:LocalService) { }
  colors=["blue","white","black","red","orange","green","#999999","#454545","#800080","#000080","#00FF00","#800000","brown","#2596be","#2596be","#be4d25"]
  current_page:any=1
  img:any
  model:any
  models:any=[]
  title = 'editor';
  file3:any
  viewimage:any
  canvas:any
  width=511;
  height=300;
  testingpicture:any;
  text:any;
  products:any=[]
  state:any;
  undo:any=[];
  redo:any=[];
 textsize=50;
 text40:any
  cpt=0;
  textalign=["left","center","justify","right"]
  hastext=false
  cacheimg=true
  cacheimg1=true
  clipart:any
  produit=false
  texte=false
  forme=false
  modele=false
  @Input() data:any
  origin_1=null;
  origin_2=null;
  face1:any
  face2:any

  Uplade(event:any){
    
    this.file3 =event.target.files[0]
    console.log(this.file3, event)
 
  const reader = new FileReader();
  reader.onload = () => {
   

   this.viewimage= reader.result;
   this.cacheimg=false
   this.cacheimg1=true
  };
  
   reader.readAsDataURL(this.file3);
 
  
     
   }

uplod(event:any){
  this.Uplade(event)
 }

 ngOnInit():void{

  console.log(this.data, 123)

  this.forms.getclipart(this.current_page).subscribe(res=>{
    this.clipart=res;

    console.log(this.clipart.cliparts)
  this.clipart=this.clipart.cliparts;
},
er=>{console.log(er)})


  this.canvas= new fabric.Canvas('aladin',{
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue',
    fireRightClick: true,
    preserveObjectStacking: true,
    stateful:true,
    stopContextMenu:false

  });
  this.canvas.filterBackend=new fabric.WebglFilterBackend(); 
  this.canvas.setWidth(this.width);
  this.canvas.setHeight(this.height);
  //this.Editor.MakeImage(this.data.url, this.canvas)
  setTimeout(()=>
  {
    this.canvas.loadFromJSON(this.data.item,()=>{
      console.log(this.data.item)
      this.canvas.setWidth(this.data.width)
      this.canvas.setHeight(this.data.height)
  
      this.canvas.requestRenderAll()
    })
  },1000)
  this.canvas.on('object:modified',() =>{
    this.save();
  });

  this.http.get().subscribe(
    res=>{
      this.model=res
      console.log(res)
      for(let item of this.model){
        this.model[this.model.indexOf(item)].obj= JSON.parse(item.obj);
        this.model[this.model.indexOf(item)].description = JSON.parse(item.description);

        this.getCanvasUrl(this.model[this.model.indexOf(item)].obj,item).then(async (res)=>{
        await setTimeout(()=>{


         },1000)
        
       }).catch((err)=>{ 
         console.log(err)
       })

      }
       
    
    
    }
  )

 }
  
 async getCanvasUrl(obj:any,item:any){

  let canvas= new fabric.Canvas(null,{
    hoverCursor: 'pointer',
    selection: true,
    selectionBorderColor:'blue',
    fireRightClick: true,
    preserveObjectStacking: true,
    stateful:true,
    stopContextMenu:false,


  });

  
 return await  canvas.loadFromJSON(obj,(ob:any)=>{
  canvas.setHeight(400)
  canvas.setWidth(400)
   var product={
    url:canvas.toDataURL(),
    price:item.description.price,
    promo:item.description.promo,
    type:item.description.type,
    name:item.description.name,
    owner:item.description.owner,
    item:item
  }     
  if(item.type!="model"){
   this.models.push(product);
   //this.products.push(product)

  }else{
    this.products.push(product)
  }
   
   
 })
}

loadCanvas(item:any){
  this.canvas.loadFromJSON(item.obj,()=>{
    console.log(item)
    this.canvas.setWidth(item.width)
    this.canvas.setHeight(item.height)

    this.canvas.requestRenderAll()
  })

}

 makeItalic(){
  this.Editor.italic(this.canvas)

}

replay(playStack:any, saveStack:any, buttonsOn:any, buttonsOff:any) {
  saveStack.push(this.state);
  this.state = playStack.pop();
  var on = $(buttonsOn);
  var off = $(buttonsOff);
  // turn both buttons off for the moment to prevent rapid clicking
  on.prop('disabled', true);
  off.prop('disabled', true);
  this.canvas.clear();
  this.canvas.loadFromJSON(this.state,()=> {
    this.canvas.renderAll();
    // now turn the buttons back on if applicable
    on.prop('disabled', false);
    if (playStack.length) {
      off.prop('disabled', false);
    }
  });
}


Redo(){
  $('#redo').click((e:any)=> {
    this.replay(this.redo, this.undo, '#undo',e);
  })
}
Undo(){
  $('#undo').click((e:any)=> {
    this.replay(this.undo,this.redo, '#redo', e);
  });
 
}

makeBold(){
  this.Editor.bold(this.canvas)   
}

underlineText(){
  this.Editor.underline(this.canvas)
}

sendBack(){
  this.Editor.sendBack(this.canvas)
}

sendForward(){
  this.Editor.sendForward(this.canvas)
}
overlineText(){
  this.Editor.overline(this.canvas)
}


addText(){
  if(!this.hastext){
    this.Editor.addText(this.canvas);
    this.hastext=true;
  }
}

duplicate(){
  this.copy();
  this.paste()
}



copy(){
  this.Editor.copy(this.canvas)
}

paste(){
  this.Editor.paste(this.canvas)
}


save() {
  this.redo = [];
  $('#redo').prop('disabled', true);

  if (this.state) {
    this.undo.push(this.state);
    $('#undo').prop('disabled', false);
  }
  this.state = JSON.stringify(this.canvas.getActiveObject());
}


textAlign(val:any){
  this.Editor.textAlign(this.canvas,val)

}

removeItem(){

  this.Editor.remove(this.canvas);
}




InputChange(Inputtext:any){
  if(this.canvas.getActiveObject()!=undefined && this.canvas.getActiveObject().text){
    if(this.cpt==0){
      this.text=this.canvas.getActiveObject().text+" "+ this.text
      this.cpt=this.cpt+1
      this.canvas.getActiveObject().text= this.text
      this.canvas.requestRenderAll();
    }else{
      this.canvas.getActiveObject().text= this.text
      this.canvas.requestRenderAll();
    }

  }else{
   
    let text= new fabric.Textbox(this.text,{
      top:200,
      left:200,
      fill:"blue",
      fontSize:38,
      fontStyle:'normal',
      cornerStyle:'circle',
      selectable:true,
      borderScaleFactor:1,
      overline:false,
      lineHeight:1.5
    });

    this.canvas.add(text).setActiveObject(text);
    this.canvas.renderAll(text);
    this.canvas.requestRenderAll();
    this.canvas.centerObject(text);
  }


}

texteclor($event:ColorEvent){
  this.Editor.textcolor($event.color.hex,this.canvas);

}



setItem(event:any){
this.Editor.setitem(event,this.canvas)
}


onFileUpload(event:any){
  let file = this.file3;
  this.cacheimg=true
  this.cacheimg1=false
  if(!this.Editor.handleChanges(file)){

    const reader = new FileReader();

  reader.onload = () => {
    let url:any = reader.result;
    fabric.Image.fromURL(url,(oImg) =>{
    oImg.set({
        scaleX:0.5,
        scaleY:0.5,
        crossOrigin: "Anonymous",
  });
    this.canvas.add(oImg).setActiveObject(oImg);
    this.canvas.centerObject(oImg);
    this.canvas.renderAll(oImg)

  })
    };
    reader.readAsDataURL(file);

   

  }

}


resize(){
  this.canvas.setWidth(this.width);
  this.canvas.setHeight(this.height);
  this.canvas.centeredScaling=true
   this.canvas.renderAll()

}


InputSize(){
  var canvasWrapper:any = document.getElementById('wrapper');
  canvasWrapper.style.width = this.width;
canvasWrapper.style.height = this.height;
this.canvas.setWidth(this.width);
this.canvas.setHeight(this.width);
}
// initial dimensions



getModel(){
  for(let item of this.canvas.getObjects()){
   item.set({lockMovementX:true})
   item.set({lockMovementY:true})
   item.set({lockScalingY:true})
   item.set({lockScalingX:true})



  // item.set({selectable:false})


  }
  this.testingpicture=this.canvas.toDataURL()
  var json = this.canvas.toJSON(['lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
 // this.testcanvas.loadFromJSON(json, this.testcanvas.renderAll.bind(this.testcanvas));
 // this.testcanvas.setHeight(this.height)
 // this.testcanvas.setWidth(this.width)


}
textwidth(){
  
}
currentPage(event:any){
  var page=event.target.id
    if(+page){
      this.current_page=page
      
      this.forms.getclipart(page).subscribe(res=>{
        let data:any=res;
        if(data.status==200){
          if(data.cliparts.length>0){
            
            this.clipart=data.cliparts
          }else{
           
          }
        
        }else{
         
        }
      }, )
    }

}

Savemodal(){
  this.face1=this.canvas.toDataURL()
  let data:any={}

        Object.assign(data,{face1:this.face1,face2:this.face1,data:this.data});
        this.local.add(data);
}
}

import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/core';

@Component({
  selector: 'app-produits',
  templateUrl: './produits.component.html',
  styleUrls: ['./produits.component.scss']
})
export class ProduitsComponent implements OnInit {
 clothes:any
 url="/editor/cloth/"
  constructor(private cltd:ListService) { }

  ngOnInit(): void {
    this.cltd.getclothespage().subscribe(
      res=>{
        this.clothes= res.data
        console.log(this.clothes)
      },
      err=>{
        console.log(err)
      }
    )
  }
 
}

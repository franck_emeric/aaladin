import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NeweditorRoutingModule } from './neweditor-routing.module';
import { NeweditorComponent } from './neweditor/neweditor.component';
import { ColorCircleModule } from 'ngx-color/circle';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    NeweditorComponent
  ],
  imports: [
    CommonModule,
    NeweditorRoutingModule,
    ColorCircleModule,
    FormsModule,
    HttpClientModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class NeweditorModule { }

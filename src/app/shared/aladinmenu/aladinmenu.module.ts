import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditortextComponent } from './editortext/editortext.component';
import { EditorproductsComponent } from './editorproducts/editorproducts.component';
import { EditormodelsComponent } from './editormodels/editormodels.component';
import { EditorimportsComponent } from './editorimports/editorimports.component';
import { EditorshapesComponent } from './editorshapes/editorshapes.component';



@NgModule({
  declarations: [
    EditortextComponent,
    EditorproductsComponent,
    EditormodelsComponent,
    EditorimportsComponent,
    EditorshapesComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    EditortextComponent,
    EditorproductsComponent,
    EditormodelsComponent,
    EditorimportsComponent,
    EditorshapesComponent
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AladinmenuModule { }

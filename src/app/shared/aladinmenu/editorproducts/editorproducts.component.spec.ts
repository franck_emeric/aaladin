import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorproductsComponent } from './editorproducts.component';

describe('EditorproductsComponent', () => {
  let component: EditorproductsComponent;
  let fixture: ComponentFixture<EditorproductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditorproductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorproductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorComponent } from './editor/editor.component';



@NgModule({
  declarations: [
    EditorComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    EditorComponent
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class AladineditorModule { }

import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { of } from 'rxjs';
//import { emit } from 'process';
import { AladinService, LocalService } from 'src/app/core';
import { AladinClipartsComponent } from 'src/app/shared/sharededitor/aladin-cliparts/aladin-cliparts.component';
declare var require: any
var myalert=require('sweetalert2')

@Component({
  selector: 'app-creation',
  templateUrl: './creation.component.html',
  styleUrls: ['./creation.component.scss']
})
export class CreationComponent implements OnInit {
  constructor(private localservice:LocalService, private uplod:AladinService) {

  }
  @Input() url:any;
  @Output() changecomponent=new EventEmitter<boolean>();

 hiderow=false;
 hide=false;
 visible=false
 oui=false;
 imagepreview:any;
 viewimage:any;
 ViewImg:any;
 file2:any;
 file3:any;
 file4:any;
 Price:any;
chanecpt=true;
t:any;
@Input() sachet:boolean=false;
@Input() sac:boolean=false;
swhsac=true;
swhsachet=true;
message="veuillez importer votre marquete svp.!";
err=false;
size_1=["15/20","20/25","25/30","28/35","35/45","45/50","50/55"];
price_1=[5000,5000,7000,7000,10000,15000,15000];
promo_1=[4000,4000,6000,6000,8000,10000,10000];
price_duo=[5000,10000,20000];
nbcolor=[1,2,3,4];
CHANGEPRICE_1=5000;
size_2=["20/30","30/30","33/40","40/45","50/60"];
price_2=[3000,5000,7000,15000,15000];
promo_2=[2500,4000,6000,10000,10000]
Cost:any;
new_cost:any;
mynbc:any
current_size_1=""
current_size_2=""
choice_1=false;
choice_2=false;
choice_3=false;
choice_4=false;

choice_5=false;
choice_6=false;
choice_7=false;
choice_8=false;
mycpt=0;
shwpr_2=false;
hascolor=false;
hasnotcolor:any;
shwpr=false;
haschangetosac=false;
haschangetosachet=false;
bagqty=100;
filename1:any
filename2:any
  ngOnInit(): void {

    document.body.scrollTop = document.body.scrollHeight;


  }

Mutiplybytwo(){
  this.bagqty=this.bagqty*2;
  this.Cost=+this.Cost*2;
  this.new_cost=+this.new_cost*2;
    this.getPrice()
}

DivideBytwo(){
  let qty=~~(this.bagqty/2)
 if(qty>0 && qty>=100){
   this.bagqty=qty;
   this.Cost=~~((+this.Cost)/2);
   this.new_cost=~~((+this.new_cost)/2);
   this.getPrice()


 }
}

getPrice(){
  if(this.current_size_2!=""){
    if(this.current_size_2==this.size_2[0]){
      this.Price=this.price_2[0];
      this.Price=this.promo_2[0];
    }

    if(this.current_size_2==this.size_2[1]){
     this.Price=this.price_2[1];
     this.Price=this.promo_2[1];

   }
   if(this.current_size_2==this.size_2[2]){
     this.Price=this.price_2[2];
     this.Price=this.promo_2[2];

   }
   if(this.current_size_2==this.size_2[3]){
     this.Price=this.price_2[3];
     this.Price=this.promo_2[3];

   }

   if(this.current_size_2==this.size_2[4]){
    this.Price=this.price_2[4];
    this.Price=this.promo_2[4];

  }


  }
   if(this.current_size_1!=""){
     if(this.current_size_1==this.size_1[0]){
       this.Price=this.price_1[0]
       this.Price=this.promo_1[0];

     }

     if(this.current_size_1==this.size_1[1]){
      this.Price=this.price_1[1]
      this.Price=this.promo_1[1];

    }
    if(this.current_size_1==this.size_1[2]){
      this.Price=this.price_1[2]
      this.Price=this.promo_1[2];

    }
    if(this.current_size_1==this.size_1[3]){
      this.Price=this.price_1[3]
      this.Price=this.promo_1[3];

    }

    if(this.current_size_1==this.size_1[4]){
      this.Price=this.price_1[4]
      this.Price=this.promo_1[4];

    }

    if(this.current_size_1==this.size_1[5]){
      this.Price=this.price_1[5]
      this.Price=this.promo_1[5];

    }
    if(this.current_size_1==this.size_1[6]){
      this.Price=this.price_1[6]
      this.Price=this.promo_1[6];

    }
   }
}


InputChangeColor(event:any){
  if(event.target.value!="Choix de le Nombre de couleur"){
    this.getPrice()
    this.mynbc=event.target.value;
    if(event.target.value==this.nbcolor[0]){
      if(this.choice_1){
        this.Cost=this.price_1[0]+this.CHANGEPRICE_1;
        this.new_cost=this.promo_1[0]+this.CHANGEPRICE_1;
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }

      if(this.choice_2){
        this.new_cost=this.promo_1[2]+this.CHANGEPRICE_1;
        this.Cost=this.price_1[2]+this.CHANGEPRICE_1;
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }

      if(this.choice_3){
        this.new_cost=this.promo_1[4]+this.CHANGEPRICE_1;
        this.Cost=this.price_1[4]+this.CHANGEPRICE_1;
        this.Cost=~~((this.bagqty)/100)*this.Cost

      }

      if(this.choice_4){
        this.new_cost=this.promo_1[5]+this.CHANGEPRICE_1;
        this.Cost=this.price_1[5]+this.CHANGEPRICE_1;
        this.Cost=~~((this.bagqty)/100)*this.Cost

      }


    }


    if(event.target.value==this.nbcolor[1]){
      if(this.choice_2){
        this.new_cost=this.promo_1[2]+(this.CHANGEPRICE_1*2);
        this.Cost=this.price_1[2]+(this.CHANGEPRICE_1*2);
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }


      if(this.choice_3){
        this.new_cost=this.promo_1[4]+(this.CHANGEPRICE_1*2);
        this.Cost=this.price_1[4]+(this.CHANGEPRICE_1*2);
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }

      if(this.choice_4){
        this.new_cost=this.promo_1[5]+(this.CHANGEPRICE_1*2);
        this.Cost=this.price_1[5]+(this.CHANGEPRICE_1*2);
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }

      if(this.choice_1){
        this.new_cost=this.promo_1[1]+(this.CHANGEPRICE_1*2);
        this.Cost=this.price_1[1]+(this.CHANGEPRICE_1*2);
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }



    }

    if(event.target.value==this.nbcolor[2]){
      if(this.choice_2){
        this.new_cost=this.promo_1[2]+(this.CHANGEPRICE_1*4);
        this.Cost=this.price_1[2]+(this.CHANGEPRICE_1*4);
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }


      if(this.choice_3){
        this.new_cost=this.promo_1[4]+(this.CHANGEPRICE_1*4);
        this.Cost=this.price_1[4]+(this.CHANGEPRICE_1*4);
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }

      if(this.choice_4){
        this.new_cost=this.promo_1[5]+(this.CHANGEPRICE_1*4);
        this.Cost=this.price_1[5]+(this.CHANGEPRICE_1*4);
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }

      if(this.choice_1){
        this.new_cost=this.promo_1[1]+(this.CHANGEPRICE_1*4);
        this.Cost=this.price_1[1]+(this.CHANGEPRICE_1*4);
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }

    }

    if(event.target.value==this.nbcolor[3]){

      if(this.choice_2){
        this.new_cost=this.promo_1[2]+(this.CHANGEPRICE_1*4);
        this.Cost=this.price_1[2]+(this.CHANGEPRICE_1*4);
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }


      if(this.choice_3){
        this.new_cost=this.promo_1[4]+(this.CHANGEPRICE_1*4);
        this.Cost=this.price_1[4]+(this.CHANGEPRICE_1*4);
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }

      if(this.choice_4){
        this.new_cost=this.promo_1[5]+(this.CHANGEPRICE_1*4);
        this.Cost=this.price_1[5]+(this.CHANGEPRICE_1*4);
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }

      if(this.choice_1){
        this.new_cost=this.promo_1[1]+(this.CHANGEPRICE_1*4);
        this.Cost=this.price_1[1]+(this.CHANGEPRICE_1*4);
        this.Cost=~~((this.bagqty)/100)*this.Cost


      }

    }

  }

}

yescolor(){
this.getPrice()
   if(this.hascolor==false){

    if(this.choice_5){
      this.new_cost=this.promo_2[0]+this.CHANGEPRICE_1;
      this.Cost=this.price_2[0]+this.CHANGEPRICE_1;
      this.Cost=~~((this.bagqty)/100)*this.Cost

    }

    if(this.choice_6){
      this.new_cost=this.promo_2[1]+this.CHANGEPRICE_1;
      this.Cost=this.price_2[1]+this.CHANGEPRICE_1;
      this.Cost=~~((this.bagqty)/100)*this.Cost

    }


    if(this.choice_7){
      this.new_cost=this.promo_2[2]+this.CHANGEPRICE_1;
      this.Cost=this.price_2[2]+this.CHANGEPRICE_1;
      this.Cost=~~((this.bagqty)/100)*this.Cost

    }

    if(this.choice_8){
      this.new_cost=this.promo_2[3]+this.CHANGEPRICE_1;
      this.Cost=this.price_2[3]+this.CHANGEPRICE_1;
      this.Cost=~~((this.bagqty)/100)*this.Cost

    }

   }

   if(this.hascolor==true){

    if(this.choice_5){
      this.Cost=this.price_2[0]
      this.new_cost=this.promo_2[0]
      this.Cost=~~((this.bagqty)/100)*this.Cost

    }

    if(this.choice_6){
      this.Cost=this.price_2[1]
      this.new_cost=this.promo_2[1]
    }


    if(this.choice_7){
      this.Cost=this.price_2[2];
      this.new_cost=this.promo_2[2]
      this.Cost=~~((this.bagqty)/100)*this.Cost


    }

    if(this.choice_8){
      this.new_cost=this.promo_2[3]
      this.Cost=this.price_2[3];
      this.Cost=~~((this.bagqty)/100)*this.Cost


    }

   }

}


InputChangeSize_2(event:any){
  this.haschangetosachet=true
  this.haschangetosac=false
  this.shwpr_2=true
    if(event.target.value!="Choix de la taille du sachet (largeur/hauteur) en cm"){
      this.current_size_2=event.target.value

      if(event.target.value==this.size_2[0]){
         this.choice_5=true;
         this.choice_6=false;
         this.choice_7=false;
         this.choice_8=false;
         this.Cost=this.price_2[0];
         this.new_cost=this.promo_2[0];
         this.Price=this.Cost

         this.Cost=~~((this.bagqty)/100)*this.Cost

         if(this.hascolor){
           this.Cost=+this.Cost + this.CHANGEPRICE_1;
           this.new_cost=+this.new_cost + this.CHANGEPRICE_1;
           this.Cost=~~((this.bagqty)/100)*this.Cost

         }

      }


      if(event.target.value==this.size_2[1]){
        this.choice_5=false;
        this.choice_6=true;
        this.choice_7=false;
        this.choice_8=false;
        this.Cost=this.price_2[1];
        this.new_cost=this.promo_2[1];
        this.Price=this.Cost

        this.Cost=~~((this.bagqty)/100)*this.Cost

        if(this.hascolor){
          this.new_cost=+this.new_cost + this.CHANGEPRICE_1;
          this.Cost=+this.Cost + this.CHANGEPRICE_1;
          this.Cost=~~((this.bagqty)/100)*this.Cost

        }

     }


     if(event.target.value==this.size_2[2]){
      this.choice_5=false;
      this.choice_6=false;
      this.choice_7=true;
      this.choice_8=false;
      this.Cost=this.price_2[2];
      this.new_cost=this.promo_2[2];
      this.Price=this.Cost

      this.Cost=~~((this.bagqty)/100)*this.Cost

      if(this.hascolor){
        this.Cost=+this.Cost + this.CHANGEPRICE_1;
        this.new_cost=+this.new_cost + this.CHANGEPRICE_1;
        this.Cost=~~((this.bagqty)/100)*this.Cost

      }

   }



   if(event.target.value==this.size_2[3]){
    this.choice_5=false;
    this.choice_6=false;
    this.choice_7=false;
    this.choice_8=true;
    this.Cost=this.price_2[3];
    this.new_cost=this.promo_2[3];
    this.Price=this.Cost

    this.Cost=~~((this.bagqty)/100)*this.Cost

    if(this.hascolor){
      this.Cost=+this.Cost + this.CHANGEPRICE_1;
      this.new_cost=+this.new_cost + this.CHANGEPRICE_1;
      this.Cost=~~((this.bagqty)/100)*this.Cost

    }

 }


 if(event.target.value==this.size_2[4]){
         this.choice_5=false;
         this.choice_6=false;
         this.choice_7=false;
         this.choice_8=true;
         this.Cost=this.price_2[4];
         this.new_cost=this.promo_2[4];
         this.Price=this.Cost

         this.Cost=~~((this.bagqty)/100)*this.Cost

         if(this.hascolor){
           this.Cost=+this.Cost + this.CHANGEPRICE_1;
           this.new_cost=+this.new_cost + this.CHANGEPRICE_1;
           this.Cost=~~((this.bagqty)/100)*this.Cost

         }

      }


    }else{
      this.Cost=0;
      this.new_cost=0;
      this.shwpr_2=false

    }



}

  InputChangeSize_1(event:any){
    this.haschangetosac=true
    this.haschangetosachet=false

    if(this.sac){
     console.log(event.target.value);

     if(event.target.value!=="Choix de la taille du sac (largeur/hauteur) en cm"){

         this.shwpr=true
         this.current_size_1=event.target.value


      if(event.target.value==this.size_1[0]){
        this.choice_1=true
        this.choice_2=false
        this.choice_3=false
        this.choice_4=false
        this.Cost=this.price_1[0];
        this.new_cost=this.promo_1[0];
        this.Price=this.Cost;
        this.Cost=~~((this.bagqty)/100)*this.Cost


        if(this.mynbc){
          if(this.mynbc==this.nbcolor[0]){
            this.Cost=+this.Cost+this.CHANGEPRICE_1;
            this.new_cost=+this.new_cost + this.CHANGEPRICE_1;
            this.Cost=~~((this.bagqty)/100)*this.Cost
          }

          if(this.mynbc==this.nbcolor[1]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*2);
            this.new_cost=+this.new_cost + (this.CHANGEPRICE_1*2);
            this.Cost=~~((this.bagqty)/100)*this.Cost



          }

          if(this.mynbc==this.nbcolor[2]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost



          }

          if(this.mynbc==this.nbcolor[3]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost;


          }



        }
        console.log(this.Cost);

      }

      if(event.target.value==this.size_1[1]){
        this.Cost=this.price_1[1]
        console.log(this.Cost);
        this.choice_1=true
        this.choice_2=false
        this.choice_4=false
        this.choice_3=false
        this.new_cost=this.promo_1[1];
        this.Price=this.Cost

        this.Cost=~~((this.bagqty)/100)*this.Cost

        if(this.mynbc){
          if(this.mynbc==this.nbcolor[0]){
            this.Cost=+this.Cost+this.CHANGEPRICE_1;
            this.new_cost=+this.new_cost+this.CHANGEPRICE_1;
            this.Cost=~~((this.bagqty)/100)*this.Cost

          }

          if(this.mynbc==this.nbcolor[1]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*2);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*2);
            this.Cost=~~((this.bagqty)/100)*this.Cost



          }

          if(this.mynbc==this.nbcolor[2]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost



          }

          if(this.mynbc==this.nbcolor[3]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost


          }



        }

      }

      if(event.target.value==this.size_1[2]){
        this.Cost=this.price_1[2];
        this.new_cost=this.promo_1[2];
        this.choice_1=false
        this.choice_2=true
        this.choice_3=false
        this.choice_4=false;
        this.Price=this.Cost
        this.Cost=~~((this.bagqty)/100)*this.Cost





        if(this.mynbc){
          if(this.mynbc==this.nbcolor[0]){
            this.Cost=+this.Cost+this.CHANGEPRICE_1;
            this.new_cost=+this.new_cost+this.CHANGEPRICE_1;
            this.Cost=~~((this.bagqty)/100)*this.Cost

          }

          if(this.mynbc==this.nbcolor[1]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*2);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*2);
            this.Cost=~~((this.bagqty)/100)*this.Cost



          }

          if(this.mynbc==this.nbcolor[2]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost



          }

          if(this.mynbc==this.nbcolor[3]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost


          }



        }


      }

      if(event.target.value==this.size_1[3]){
        this.Cost=this.price_1[3]
        this.new_cost=this.promo_1[3];
        console.log(this.Cost);
        this.choice_1=false
        this.choice_2=true
        this.choice_3=false
        this.choice_4=false;
        this.Price=this.Cost;

        this.Cost=~~((this.bagqty)/100)*this.Cost

        if(this.mynbc){
          if(this.mynbc==this.nbcolor[0]){
            this.Cost=+this.Cost+this.CHANGEPRICE_1;
            this.new_cost=+this.new_cost+this.CHANGEPRICE_1;
            this.Cost=~~((this.bagqty)/100)*this.Cost

          }

          if(this.mynbc==this.nbcolor[1]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*2);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*2);
            this.Cost=~~((this.bagqty)/100)*this.Cost



          }

          if(this.mynbc==this.nbcolor[2]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost



          }

          if(this.mynbc==this.nbcolor[3]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost


          }



        }


      }

      if(event.target.value==this.size_1[4]){
        this.Cost=this.price_1[4];
        this.new_cost=this.promo_1[4];
        console.log(this.Cost);
        this.choice_1=false
        this.choice_2=false
        this.choice_3=true
        this.choice_4=false;
        this.Price=this.Cost

        this.Cost=~~((this.bagqty)/100)*this.Cost

        if(this.mynbc){
          if(this.mynbc==this.nbcolor[0]){
            this.Cost=+this.Cost+this.CHANGEPRICE_1;
            this.new_cost=+this.new_cost+this.CHANGEPRICE_1;
            this.Cost=~~((this.bagqty)/100)*this.Cost

          }

          if(this.mynbc==this.nbcolor[1]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*2);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*2);
            this.Cost=~~((this.bagqty)/100)*this.Cost



          }

          if(this.mynbc==this.nbcolor[2]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost



          }

          if(this.mynbc==this.nbcolor[3]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost


          }



        }

      }


      if(event.target.value==this.size_1[5]){
        this.Cost=this.price_1[5];
        this.new_cost=this.promo_1[5];
        console.log(this.Cost);
        this.choice_1=false
        this.choice_2=false
        this.choice_3=false
        this.choice_4=true;
        this.Price=this.Cost

        this.Cost=~~((this.bagqty)/100)*this.Cost

        if(this.mynbc){
          if(this.mynbc==this.nbcolor[0]){
            this.Cost=+this.Cost+this.CHANGEPRICE_1;
            this.new_cost=+this.new_cost+this.CHANGEPRICE_1;
            this.Cost=~~((this.bagqty)/100)*this.Cost

          }

          if(this.mynbc==this.nbcolor[1]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*2);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*2);
            this.Cost=~~((this.bagqty)/100)*this.Cost



          }

          if(this.mynbc==this.nbcolor[2]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost



          }

          if(this.mynbc==this.nbcolor[3]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost


          }



        }

      }


      if(event.target.value==this.size_1[6]){
        this.Cost=this.price_1[6];
        this.new_cost=this.promo_1[6];
        console.log(this.Cost);
        this.choice_1=false
        this.choice_2=false
        this.choice_3=false
        this.choice_4=true;
        this.Price=this.Cost
        this.Cost=~~((this.bagqty)/100)*this.Cost

        if(this.mynbc){
          if(this.mynbc==this.nbcolor[0]){
            this.Cost=+this.Cost+this.CHANGEPRICE_1;
            this.new_cost=+this.new_cost+this.CHANGEPRICE_1;
            this.Cost=~~((this.bagqty)/100)*this.Cost

          }

          if(this.mynbc==this.nbcolor[1]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*2);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*2);
            this.Cost=~~((this.bagqty)/100)*this.Cost

          }

          if(this.mynbc==this.nbcolor[2]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost


          }

          if(this.mynbc==this.nbcolor[3]){
            this.Cost=+this.Cost+(this.CHANGEPRICE_1*4);
            this.new_cost=+this.new_cost+(this.CHANGEPRICE_1*4);
            this.Cost=~~((this.bagqty)/100)*this.Cost


          }



        }

      }

     }else{
      this.Cost=0;
      this.new_cost=0;
      this.shwpr=false;
    }
    }
  }

  Isachet(){
    this.current_size_1="";
    this.current_size_2="";
    this.bagqty=100;
    this.shwpr_2=false;
    this.hascolor=false;
    this.haschangetosachet=false
    this.haschangetosac=false
    if(this.sachet){
      this.sac=false;
      this.sachet=true;
      this.swhsac=true;
      this.swhsachet=true;

    }else{
      this.sachet=true;
      this.sac=false;
      this.swhsac=false;
      this.swhsachet=true;

    }

   }

   Isac(){
    this.bagqty=100
    this.current_size_1=""
    this.current_size_2=""
    this.shwpr=false
    this.haschangetosachet=false
    this.haschangetosac=false
     if(this.sac){
      this.sac=false;
      this.sachet=false;
      this.swhsac=true
      this.swhsachet=true;

     }else{
      this.sac=true;
      this.sachet=false;
      this.swhsac=true
      this.swhsachet=false;


     }
   }


   Uplode(event:any){
    this.file2 =event.target.files[0]
    if(!this.uplod.UpleadImage(this.file2)){
    const reader = new FileReader();

    reader.onload = () => {

    this.imagepreview = reader.result;
    
   };

   reader.readAsDataURL(this.file2);
    console.log(event);
  }else{
    
  }

   }
  View(){
    this.visible=true;
    this.oui=!this.oui

  }
  view2(){
    this.visible=false
  }



  Uplade(event:any){
    this.file3 =event.target.files[0]
    if(this.file3.type=="application/pdf"){
      const reader = new FileReader();
   
      reader.onload = () => {
        
       this.viewimage= reader.result;
        this.filename1=this.file3.name

       };
       reader.readAsDataURL(this.file3);
      }else{
        myalert.fire({
          title: "Désolé!!!!!!",
          text: "Veuillez importer vos maquettes en PDF SVP!!!!! ",
          icon: "error",
           button: "Ok"
          });
      }
  
  
  

   }
   Upladeimage(event:any){
    this.file4 =event.target.files[0]
    if(this.file4.type=="application/pdf"){
      const reader = new FileReader();
      reader.onload = () => {
    
       this.ViewImg= reader.result;
       this.filename2=this.file4.name
       };
       
   reader.readAsDataURL(this.file4);
   console.log(event)
    }else{
      myalert.fire({
        title: "Désolé!!!",
        text: "Veuillez importer vos maquettes en PDF SVP!!!!! ",
        icon: "error",
         button: "Ok"
        });
    }
   


   }


  Addtocart = ()=>{

    var cart:any={};


console.log(this.Price)
    if(this.url!=undefined && this.viewimage!=undefined && this.Price){
      cart ={
        type_product:"crea",
        t:+this.new_cost,
        category:"emballage",
        face1:this.url,
        face2:this.imagepreview,
        f3:this.viewimage,
        f4:this.ViewImg,
        qty:this.bagqty,
        price:this.Price,

      };



      if(this.current_size_1 !=""){

        Object.assign(cart,{name:"sac",color:this.mynbc,size:this.current_size_1}
      )

      }

      if(this.current_size_2!=""){

        if(this.hascolor==true){
          Object.assign(cart,
             {name:"sachet",color:"oui",size:this.current_size_2}
          
        );


        }else{
          Object.assign(cart, 
            
              {name:"sachet",color:"non",size:this.current_size_2,}
            
          
            
        );

        

      }
    }
         try{
        this.localservice.adtocart(cart);
           location.href="cart/"

      }catch(e:any){
        console.log(e)
      }
      console.log(cart);




    }





  }

ChangeComponent(value:boolean){
  this.changecomponent.emit(value);
  this.sac=false;
  this.sachet=false;

}
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HommesComponent } from './hommes/hommes.component';
import { FemmesComponent } from './femmes/femmes.component';
import { EnfantsComponent } from './enfants/enfants.component';



@NgModule({
  declarations: [
    HommesComponent,
    FemmesComponent,
    EnfantsComponent
  ],
  imports: [
    CommonModule
  ]
})
export class TeeshirtsModule { }

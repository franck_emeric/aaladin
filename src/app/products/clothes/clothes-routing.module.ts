import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { ClothesmanagerComponent } from './clothesmanager/clothesmanager.component';

const routes: Routes = [
 {path:'/', component:ClothesmanagerComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClothesRoutingModule { }

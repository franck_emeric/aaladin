import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AladindisplaysRoutingModule } from './aladindisplays-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AladindisplaysRoutingModule
  ]
})
export class AladindisplaysModule { }

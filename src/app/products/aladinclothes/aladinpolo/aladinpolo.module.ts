import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AladinpoloRoutingModule } from './aladinpolo-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AladinpoloRoutingModule
  ]
})
export class AladinpoloModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChasublesComponent } from './chasubles.component';

describe('ChasublesComponent', () => {
  let component: ChasublesComponent;
  let fixture: ComponentFixture<ChasublesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChasublesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChasublesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

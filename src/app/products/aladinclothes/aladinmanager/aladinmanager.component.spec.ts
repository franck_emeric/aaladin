import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AladinmanagerComponent } from './aladinmanager.component';

describe('AladinmanagerComponent', () => {
  let component: AladinmanagerComponent;
  let fixture: ComponentFixture<AladinmanagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AladinmanagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AladinmanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AladinclothesRoutingModule } from './aladinclothes-routing.module';
import { ChasublesComponent } from './chasubles/chasubles.component';
import { AladinmanagerComponent } from './aladinmanager/aladinmanager.component';
import { SharedModule } from 'src/app/shared';


@NgModule({
  declarations: [
    ChasublesComponent,
    AladinmanagerComponent
  ],
  imports: [
    CommonModule,
    AladinclothesRoutingModule,
    SharedModule
  ]
})
export class AladinclothesModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AladinprintedRoutingModule } from './aladinprinted-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AladinprintedRoutingModule
  ]
})
export class AladinprintedModule { }

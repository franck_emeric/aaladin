import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.scss']
})
export class DescriptionComponent implements OnInit {
  @Output() changeComponent=new EventEmitter<boolean>();
  @Input() data:any;
  url='/editor/prints/';
  Changevalue=true;
  constructor() { }

  ngOnInit(): void {
  }
  reload(value:boolean){
    this.changeComponent.emit(value);
    this.data.show=false;
  }
}
